import React from "react";
import logo from "./logo.svg";
import "./App.css";
import ForecastList from "./Components/ForecastList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ForecastList></ForecastList>
      </header>
    </div>
  );
}

export default App;
