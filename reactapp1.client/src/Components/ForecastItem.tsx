import React from "react";
import "./ForecastItem.css";

const ForecastItem = (props: any) => {
  return (
    <div className="forecastitem">
      <div>Date: {props.date}</div>
      <div>Temperature C:{props.temperatureC}</div>
      <div>Temperature F{props.temperatureF}</div>
      <div>Summary: {props.summary}</div>
    </div>
  );
};

export default ForecastItem;
