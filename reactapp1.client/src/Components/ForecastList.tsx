import React, { useEffect, useState } from "react";
import ForecastItem from "./ForecastItem";
import axios from "axios";
import "./ForecastList.css";

const ForecastList = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const apiUrl = "http://localhost:8088/WeatherForecast";
    axios.get(apiUrl).then((resp) => {
      console.log(resp);
      setData(resp.data);
    });
  }, [setData]);

  return (
    <div className="forecastcontainer">
      {data.map((item: any, index) => {
        return (
          <ForecastItem
            key={index}
            date={item.date}
            temperatureC={item.temperatureC}
            temperatureF={item.temperatureF}
            summary={item.summary}
          ></ForecastItem>
        );
      })}
    </div>
  );
};

export default ForecastList;
